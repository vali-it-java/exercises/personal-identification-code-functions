package com.valiit;

public class Functions {

    private static final int[] STEP1_WEIGHTS = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
    private static final int[] STEP2_WEIGHTS = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};

    public static void main(String[] args) {
        System.out.println("Sugu: " + getGender("37605030299"));
        System.out.println("Sünniaasta: " + getBirthYear("37605030299"));
        System.out.println("Sünnikuu: " + getBirthMonth("37605030299"));
        System.out.println("Sünnikuupäev: " + getBirthDayOfMonth("37605030299"));
        System.out.println("Kas kontrollnumber on korrektne: " + isPersonalIdentificationCodeCheckNumberCorrect("37605030299"));
    }

    private static Gender getGender(String personalCode) {
        return Character.getNumericValue(personalCode.charAt(0)) % 2 == 1 ? Gender.MALE : Gender.FEMALE;
    }

    private static int getBirthYear(String personalCode) {
        int firstDigit = Character.getNumericValue(personalCode.charAt(0));
        int birthYearLastDigits = Integer.parseInt(personalCode.substring(1, 3));

        switch (firstDigit) {
            case 1:
            case 2:
                return 1800 + birthYearLastDigits;
            case 3:
            case 4:
                return 1900 + birthYearLastDigits;
            case 5:
            case 6:
                return 2000 + birthYearLastDigits;
        }
        return 0;
    }

    private static String getBirthMonth(String personalCode) {
        int month = Integer.parseInt(personalCode.substring(3, 5));
        switch (month) {
            case 1:
                return "Jaanuar";
            case 2:
                return "Veebruar";
            case 3:
                return "Märts";
            case 4:
                return "Aprill";
            case 5:
                return "Mai";
            case 6:
                return "Juuni";
            case 7:
                return "Juuli";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "Oktoober";
            case 11:
                return "November";
            case 12:
                return "Detsember";
            default:
                return "Viga!";
        }
    }

    private static int getBirthDayOfMonth(String personalCode) {
        return Integer.parseInt(personalCode.substring(5, 7));
    }

    private static boolean isPersonalIdentificationCodeCheckNumberCorrect(String personalCode) {
        int checkNumber = Character.getNumericValue(personalCode.charAt(10));

        int checkNumberCandidate = getCheckNumberCandidate(personalCode, STEP1_WEIGHTS);
        if (checkNumberCandidate != 10) {
            return checkNumber == checkNumberCandidate;
        }

        checkNumberCandidate = getCheckNumberCandidate(personalCode, STEP2_WEIGHTS);
        if (checkNumberCandidate != 10) {
            return checkNumber == checkNumberCandidate;
        } else {
            return checkNumber == 0;
        }
    }

    private static int getCheckNumberCandidate(String personalCode, int[] weights) {
        int sum = 0;
        for (int i = 0; i < personalCode.length() - 1; i++) {
            sum += Character.getNumericValue(personalCode.charAt(i)) * weights[i];
        }
        return sum % 11;
    }

    enum Gender {
        MALE, FEMALE
    }
}
